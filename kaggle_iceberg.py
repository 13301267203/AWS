

# AMI ID Deep Learning AMI Ubuntu Linux - 2.2_Aug2017 (ami-599a7721)   -我经常用的一个GPU实例，包含keras, tensorflow， maxnet, anaconda等。

# cd 到你的密钥文件所在的文件夹
# sudo chmod 400 yourkey.pem    #可能需要这步，更改密钥文件的权限

# ssh -i yourkey.pen ubuntu@123.123.123.123
# 你的用户名可能是ubuntu, ec2-user, root 等等。
# 123.123.123.123是服务器的IP地址
# 也可以使用类似这样的地址： ec2-34-208-48-45.us-west-2.compute.amazonaws.com

# nvidia-smi 查看服务器GPU信息

# sudo pip3 install wget   -安装wget库
# sudo pip3 install --upgrade keras   -更新keras
# suo pip3 install tensorflow-gpu    - 安装gpu版本 tensorflow
# sudo apt install p7zip   -安装用来解压.7z文件的库： p7zip
# p7zip -d something.7z   -解压.7z文件



import numpy as np
import pandas as pd 
from keras.preprocessing import image
from keras.models import Model
from keras.layers import Dense, GlobalAveragePooling2D, MaxPooling2D, Reshape, UpSampling2D, Conv2D, Dropout, GlobalMaxPooling2D
from keras.layers.normalization import BatchNormalization
from keras.layers import Input
from keras.optimizers import SGD, Adam
from keras.callbacks import ModelCheckpoint
from keras.utils import plot_model

#%%
def get_model():
    inputs = Input(shape=(75, 75, 3))
    
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(inputs)
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
    pool1 = Dropout(0.1)(pool1)

    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)
    pool2 = Dropout(0.3)(pool2)

    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)
    pool3 = Dropout(0.5)(pool3)


    conv5 = Conv2D(256, (3, 3), activation='relu', padding='same')(pool3)
    conv5 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv5)
    x = GlobalMaxPooling2D() (conv5)
    x = Dropout(0.5)(x)
    x = Dense(128, activation="relu")(x)
    x = Dense(64, activation="relu")(x)
    x = Dense(32, activation="relu")(x)
    output = Dense(1, activation="sigmoid")(x)
          
    
    model = Model(inputs,  output)
    optimizer = Adam(lr=0.001, decay=1e-2)
    model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=["accuracy"])
    return model


#%%
train = pd.read_json("train.json")
test = pd.read_json("test.json")

x_band1 = np.array([np.array(band).astype(np.float32).reshape(75, 75) for band in train["band_1"]])
x_band2 = np.array([np.array(band).astype(np.float32).reshape(75, 75) for band in train["band_2"]])
X_train = np.concatenate([x_band1[:, :, :, np.newaxis]
                          , x_band2[:, :, :, np.newaxis]
                         , ((x_band1+x_band1)/2)[:, :, :, np.newaxis]], axis=-1)
X_train = (X_train - X_train.mean())/X_train.std()
y_train = np.array(train["is_iceberg"])
print('*** x_train shape: ', X_train.shape)
print('*** y_train shape:', y_train.shape)


x_band1 = np.array([np.array(band).astype(np.float32).reshape(75, 75) for band in test["band_1"]])
x_band2 = np.array([np.array(band).astype(np.float32).reshape(75, 75) for band in test["band_2"]])
X_test = np.concatenate([x_band1[:, :, :, np.newaxis]
                          , x_band2[:, :, :, np.newaxis]
                         , ((x_band1+x_band1)/2)[:, :, :, np.newaxis]], axis=-1)
X_test = (X_test - X_test.mean())/X_test.std()
print('*** x_test shape: ', X_test.shape)


model = get_model()
plot_model(model, to_file='model.png')

checkpoint = ModelCheckpoint('save-{epoch:02d}-{val_loss:.4f}.hdf5',
                                 monitor = 'val_loss', 
                                 verbose = 1, 
                                 save_best_only = False, 
                                 save_weights_only = False, 
                                 mode='auto', 
                                 period = 9)
#history = model.fit(X_train, 
#          y_train, 
#          batch_size = 64, 
#          epochs = 100, 
#          verbose = 1, 
#          shuffle = True,
#          validation_split = 0.2,
#          callbacks = [checkpoint])

#print(history.history.keys())
#
#fig = plt.figure()
#plt.plot(history.history['acc'])
#plt.plot(history.history['val_acc'])
#plt.title('model accuracy')
#plt.ylabel('accuracy')
#plt.xlabel('epoch')
#plt.legend(['train', 'test'], loc='upper left')
#
#plt.plot(history.history['loss'])
#plt.plot(history.history['val_loss'])
#plt.title('model loss')
#plt.ylabel('loss')
#plt.xlabel('epoch')
#plt.legend(['train', 'test'], loc='upper left')
#
#fig.savefig('loss.png')

#%% prediction

model.load_weights('save-45-0.2365.hdf5')   # 加载训练好的参数
prediction_list=[]
prediction = model.predict(X_test, verbose=1, batch_size=100)
prediction_list.append(prediction)
submission = pd.DataFrame({'id': test["id"], 'is_iceberg': prediction.reshape((prediction.shape[0]))})
submission.to_csv("submission.csv", index=False)






